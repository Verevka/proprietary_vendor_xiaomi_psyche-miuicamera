#
# Copyright (C) 2020 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Permissions
PRODUCT_COPY_FILES += \
    vendor/xiaomi/psyche-miuicamera/configs/default-permissions/miuicamera-permissions.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/default-permissions/miuicamera-permissions.xml \
    vendor/xiaomi/psyche-miuicamera/configs/permissions/privapp-permissions-miuicamera.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-miuicamera.xml \
    vendor/xiaomi/psyche-miuicamera/configs/device_features/psyche.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/device_features/psyche.xml

# Sysconfig
PRODUCT_COPY_FILES += \
    vendor/xiaomi/psyche-miuicamera/configs/sysconfig/miuicamera-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/miuicamera-hiddenapi-package-whitelist.xml

# Product Props
PRODUCT_PRODUCT_PROPERTIES += \
	ro.com.google.lens.oem_camera_package=com.android.camera

# System Props
PRODUCT_SYSTEM_PROPERTIES += \
    persist.vendor.camera.privapp.list=com.android.camera \
    ro.product.manufacturer=Xiaomi \
    ro.product.mod_device=psyche_global \
    ro.miui.ui.version.code=14

# Vendor Props
PRODUCT_VENDOR_PROPERTIES += \
    persist.vendor.camera.eis.enable=1 \
    persist.vendor.camera.expose.aux=1 \
    persist.vendor.camera.facepp.fdenable=0 \
    persist.vendor.camera.perflock.enable=0 \
    persist.vendor.camera.facepp.fdenable=0 \
    persist.vendor.camera.enableAdvanceFeatures=0x3E7 \
    persist.vendor.camera.multicam=TRUE \
    persist.vendor.camera.multicam.fpsmatch=TRUE \
    persist.vendor.camera.multicam.framesync=1 \
    persist.vendor.camera.multicam.hwsync=TRUE \
    persist.vendor.camera.picturesize.limit.enable=false \
    persist.vendor.camera.hdrMotion=0 \
    persist.vendor.camera.dumpPreview=0

$(call inherit-product, vendor/xiaomi/psyche-miuicamera/products/board.mk)
$(call inherit-product, vendor/xiaomi/psyche-miuicamera/common/common-vendor.mk)
